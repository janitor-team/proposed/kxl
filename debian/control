Source: kxl
Section: devel
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Sam Hocevar <sho@debian.org>,
 Barry deFreese <bdefreese@debian.org>
Build-Depends:
 debhelper (>= 10),
 libx11-dev
Standards-Version: 4.3.0
Homepage: https://tracker.debian.org/pkg/kxl
Vcs-Git: https://salsa.debian.org/games-team/kxl.git
Vcs-Browser: https://salsa.debian.org/games-team/kxl

Package: libkxl0
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: multimedia library for game development
 KXL (Kacchan X Windows System Library) is a library targeted at game
 development that provides functions for simple image and sound output
 as well as higher level functions for text drawing, timer and events
 handling and image manipulation.
 .
 This package contains the libkxl0 runtime library.

Package: libkxl0-dev
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends},
 libkxl0 (= ${binary:Version}),
 libx11-dev
Description: development files for libkxl0
 KXL (Kacchan X Windows System Library) is a library targeted at game
 development that provides functions for simple image and sound output
 as well as higher level functions for text drawing, timer and events
 handling and image manipulation.
 .
 This package contains the header files and static library needed to
 compile applications that use libkxl0.
