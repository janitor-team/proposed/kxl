#include <stdio.h>
#include "KXL.h"

extern KXL_Window *KXL_Root;

//==============================================================
//  Create bitmap 8bpp to 16bpp
//  arguments : 8bpp data
//            : 16bpp image
//            : palette data
//            : blend no.
//==============================================================
void KXL_CreateBitmap8to16(Uint8 *from, XImage *to, KXL_RGBE *rgb, Uint8 blend)
{
  Uint32 x, y, offset, no;

  for (y = 0; y < to->height; y ++) {
    for (x = 0; x < to->width; x ++) {
      offset = (y * to->bytes_per_line) + (x << 1);
      no = from[y * to->width + x];
      if (no == blend) {
        to->data[offset ++] = 0x00;
        to->data[offset ++] = 0x00;
      } else {
        // 000rrrrr, 000ggggg, 000bbbbb
        //            |
        // gg0bbbbb, rrrrrggg
        if (!(rgb[no].r | rgb[no].g | rgb[no].b)) {
          to->data[offset++] = 0x41;
          to->data[offset++] = 0x08;
        } else {
          to->data[offset++] = rgb[no].b | (rgb[no].g << 6);
          to->data[offset++] = (rgb[no].r << 3) | (rgb[no].g >> 2);
        }
      }
    }
  }
}

//==============================================================
//  Create bitmap 8bpp to 24bpp
//  arguments : 8bpp data
//            : 24bpp image
//            : palette data
//            : blend no.
//==============================================================
void KXL_CreateBitmap8to24(Uint8 *from, XImage *to, KXL_RGBE *rgb, Uint8 blend)
{
  Uint32 x, y, offset, no;

  for (y = 0; y < to->height; y ++) {
    for (x = 0; x < to->width; x ++) {
      offset = (y * to->bytes_per_line) + ((x * to->bits_per_pixel) >> 3);
      no = from[y * to->width + x];
      if (no == blend) {
        to->data[offset ++] = 0x00;
        to->data[offset ++] = 0x00;
        to->data[offset ++] = 0x00;
      } else {
        if (!(rgb[no].r | rgb[no].g | rgb[no].b)) {
          to->data[offset ++] = 0x01;
          to->data[offset ++] = 0x01;
          to->data[offset ++] = 0x01;
        } else {
          to->data[offset ++] = rgb[no].b;
          to->data[offset ++] = rgb[no].g;
          to->data[offset ++] = rgb[no].r;
        }
      }
    }
  }
}

//==============================================================
//  Create bitmap 8bpp to 1bpp
//  arguments : 8bpp data
//            : 1bpp image
//            : blend no.
//==============================================================
void KXL_CreateBitmap8to1(Uint8 *from, XImage *to, Uint8 blend)
{
  Uint16 x, y, offset, no;
  
  for (y = 0; y < to->height; y ++) {
    for (x = 0; x < to->width; x ++) {
      offset = (y * to->bytes_per_line) + (x >> 3);
      no = from[y * to->width + x];
      if (no != blend)
        to->data[offset] |= 1 << (x & 7);
      else
        to->data[offset] &= ~(1 << (x & 7));
    }
  }
}

//==============================================================
//  Read bitmap header
//  arguments : file name
//            : bitmap header struct
//==============================================================
void KXL_ReadBitmapHeader(Uint8 *filename, KXL_BitmapHeader *hed)
{
  FILE *fp;
  Uint16 i, j;
  Uint8  data;

  if ((fp = fopen(filename,"rb")) == 0) {
    fprintf(stderr, "KXL error message\n'%s' is open error\n", filename);
    exit(1);
  }
  fread(hed->magic, 1, 2, fp);
  if (hed->magic[0] != 'B' || hed->magic[1] != 'M') {
    fprintf(stderr, "KXL error message\n'%s' is not bitmap file\n", filename);
    exit(1);
  }
  hed->file_size  = KXL_ReadU32(fp);
  hed->reserved1  = KXL_ReadU16(fp);
  hed->reserved2  = KXL_ReadU16(fp);
  hed->offset     = KXL_ReadU32(fp);
  hed->hed_size   = KXL_ReadU32(fp);
  hed->width      = KXL_ReadU32(fp);
  hed->height     = KXL_ReadU32(fp);
  hed->plane      = KXL_ReadU16(fp);
  hed->depth      = KXL_ReadU16(fp);
  // 4 or 8bpp is not support
  if (hed->depth < 4 || hed->depth > 8) {
    fprintf(stderr, "KXL error message\n'%s' %dbps not support\n", 
            filename, hed->depth);
    exit(1);
  }
  hed->lzd        = KXL_ReadU32(fp);
  hed->image_size = KXL_ReadU32(fp);
  if (hed->image_size == 0) {
    fprintf(stderr, "KXL error message\n'%s not found image size\n",
            filename);
    exit(1);
  }
  hed->x_pixels   = KXL_ReadU32(fp);
  hed->y_pixels   = KXL_ReadU32(fp);
  hed->pals       = KXL_ReadU32(fp);
  hed->pals2      = KXL_ReadU32(fp);
  // used paletts
  hed->pals = hed->pals ? hed->pals : (1 << hed->depth);
  // get color map
  hed->rgb = (KXL_RGBE *)KXL_Malloc(sizeof(KXL_RGBE) * hed->pals);
  for (i = 0; i < hed->pals; i ++) {
    hed->rgb[i].b = fgetc(fp);
    hed->rgb[i].g = fgetc(fp);
    hed->rgb[i].r = fgetc(fp);
    hed->rgb[i].e = fgetc(fp);
    if (KXL_Root->Depth == 16) {
      hed->rgb[i].b /= 8;
      hed->rgb[i].g /= 8;
      hed->rgb[i].r /= 8;
    }
  }
  hed->w = ((hed->width + 3) / 4) * 4;
  if (hed->depth == 8)
    hed->data = (Uint8 *)KXL_Malloc(hed->image_size);
  else
    hed->data = (Uint8 *)KXL_Malloc(hed->image_size * 2);
  if (hed->depth == 8) {
    for (i = 0; i < hed->height; i++) {
      fseek(fp, hed->offset + (hed->height - i - 1) * hed->w, 0); 
      fread(&(hed->data[i * hed->w]), hed->w, 1, fp);
    }
  } else {
    Uint32 w = (((hed->width / 2) + 3) / 4) * 4;
    for (i = 0; i < hed->height; i++) {
      fseek(fp, hed->offset + (hed->height - i - 1) * w, 0); 
      for (j = 0; j < w; j ++) {
        data = fgetc(fp);
        hed->data[i * hed->w + j * 2 + 0] = data >> 4;
        hed->data[i * hed->w + j * 2 + 1] = data & 0x0f;
      }
    }
  }
  hed->depth = 8;
  fclose(fp);
}
